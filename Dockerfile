FROM manjarolinux/base
RUN useradd build
RUN echo 'build:build' | chpasswd
RUN echo 'build ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
COPY makepkg.conf /etc/makepkg.conf
COPY mirrorlist /etc/pacman.d/mirrorlist
RUN pacman -Syyu --noconfirm
RUN pacman --noconfirm -S git vi vim base-devel
RUN git clone https://aur.archlinux.org/chromium-vaapi.git
COPY run.sh /chromium-vaapi/run.sh
RUN chown -R build.build /chromium-vaapi
# fix glibc 2.33 on old docker engines
RUN patched_glibc=glibc-linux4-2.33-4-x86_64.pkg.tar.zst && \
    curl -LO "https://repo.archlinuxcn.org/x86_64/$patched_glibc" && \
    bsdtar -C / -xvf "$patched_glibc"
USER build
WORKDIR /chromium-vaapi
ENTRYPOINT ["bash", "run.sh"]
