#!/bin/bash

# for rebuilding 90.0.4430.85-1 as 90.0.4430.85-3
sed -i '/pkgrel=1/s/1/3/' PKGBUILD

time makepkg -s --noconfirm
PKG=$(find /chromium-vaapi -name '*.pkg.tar.xz' -type f)
echo
echo "docker cp $(cat /etc/hostname):$PKG ."
