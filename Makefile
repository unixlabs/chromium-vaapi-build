all: build tag push
version := $(shell cat version)
build:
	docker pull manjarolinux/base
	docker build --no-cache -t chromium-vaapi-build .
tag:
	docker tag chromium-vaapi-build registry.gitlab.com/unixlabs/chromium-vaapi-build:${version}
	docker tag chromium-vaapi-build registry.gitlab.com/unixlabs/chromium-vaapi-build:latest
push:
	docker push registry.gitlab.com/unixlabs/chromium-vaapi-build:${version}
	docker push registry.gitlab.com/unixlabs/chromium-vaapi-build:latest
